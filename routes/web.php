<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/ruta/insert','ControladorRuta@insert');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


////////////////////////////////////////////////////////////////////////////7/////MIDDLEWARE///////////////////////////////7

Route::group(['middleware' =>'auth'],function (){

//////////////////7///// 2DO PASO /////////////TOKEN PUBLICO Y PRIVADO////////////////////
    Route::get('/tokenguardado', 'GuardarTokenController@token');

    Route::get('/tokenpublicoyprivado',function(){
        return view ('tokenPyB');
        
    });

////////////////////////////////////////////////////////////3ER PASO    
    Route::get('/callback','CallbackController@callback');

});

///////////////////////////////////////////////////////////////////////////////////CONTROLADORES

////// 1ER PASO////////////////////CONTRASEÑA Y USUARIO///
Route::get('/istrid', 'IstridControler@usuarios');     ///
Route::get('/istridformulario', function(){            ///
    return view ('FormularioIstris');                  ///
});                                                    ///
//////////////////////////////////////////////////////////

///////////////////////////////////////////////////4TO PASO



Route::get('/api/insertarcontactos', 'InsertarController@insertar');

Route::get('/salesforce/conexion', 'RedirectController@redirect');

//////////////////////////////////////ENVIO DE SMS//////////////
Route::post('/twilio','ControladorTwilio@sms');

Route::get('/twilio/sms', function(){
    return view('mensaje');
});





