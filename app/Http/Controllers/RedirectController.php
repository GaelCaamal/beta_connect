<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RedirectController extends Controller
{
    public function redirect(){

        $user = Auth::user();
        $user_id = $user->id;

        DB::table('credentials')->where('user_id',$user_id)->get();
        $consulta_credencial = DB::table('credentials')->where('user_id',$user_id)->get();

        if($consulta_credencial=="[]" ){ //se busca un objeto vacio "[]"
            return view('tokenPyB');
        }else{
            return view('yaconexion');
            return 'ya estas conectado';
        }
    }
}
