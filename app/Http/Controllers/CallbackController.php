<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use illiminate\Support\Facades\Log;//se utiliza unicamente en el log que contiene el try cach

class CallbackController extends Controller
{
    public function callback(Request $request){
        $direccionurl2 =Controller::$direccionurl;
      //  try{
            $code = $request['code'];
            $user = Auth::user();
            $user_id = $user->id;

            if(isset($code)){
                
            }
            else{
                DB::table('saleforce')->where('username',$user_id)->delete();
            }
            $consultas = DB::table('saleforce')->where('username',$user_id);
            $tokenpublica = $consultas->consumer_key;
            $tokenprivada = $consultas->consumer_secret;
            $client = new Client([
                'headers'=>['content-type'=>'application/json','Accept'=>'application/json'],
            ]);

            $responce = $client->request('POST','http://login.salesforce.com/services/oauth2/token',[
                'form_param'=>[
                    'grant_type'=>'authorization_code',
                    'client_secret'=>$tokenprivada,
                    'client_id'=>$tokenpublica,
                    'redirect_uri' => $direccionurl2.'/callback',
                    'code' => $code
                ],
            ]);
                          
                Log::info($responce->getBody());
                $api_data = json_decode($responce->getBody());
                //console.log($api_data);
                $access_token = $api_data->access_token;
                $token_type = $api_data->token_type;
                $refresh_token = $api_data->refresh_token;
                $instance_url =$api_data->instance_url;
                $id = $api_data->id;
                $issued_at = $api_data->issued_at;
                $signature = $api_data->signature;

                DB::table('credentials')->insert(array('access_token'=>$access_token,
                'token_type'=>$token_type,'refresh_token'=>$refresh_token, 'instance_url'=>$instance_url,
                'id'=>$id, 'issuef_at'=>$issued_at, 'signature'=>$signature 
            ));
            return redirect ('/salesforce/conexion');
        //}catch(RequestException $e){
            //log::error($e);

        }

    }

